#include <stdio.h>

int main(){
    system("clear");
    int a = 2;
    int b = 4;
    printf("Rekursif %d pangkat %d adalah : %d", a, b, rekursif(a,b));
    printf("\nRekursif faktorial %d adalah : %d", b, faktorial(1));
    printf("\nRekursif fibonacci %d adalah : %d", b, fibonacci(b));
}

int rekursif(int a, int b){ // Rekursif a pangkat b
    if(b<=1){
        return a;
    }else
    {
        return a * rekursif (a,(b-1));
    }   
}

int faktorial(int a){ // Rekursif b faktorial 
    if(a<=1){
        return a;
    }else{
        return a * faktorial(a-1);
    }
}

int fibonacci(int a){ // Rekursif fibbonacci ke b
    if(a==0|| a==1){
        return a;
    }else{
        return fibonacci(a-1)+fibonacci(a-2);
    }
}